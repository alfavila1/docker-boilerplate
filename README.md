# Docker Boilerplate
## Steps for create Laravel Project
* `.env` - Edit configuration file
* `make init` - Setup, first time only
* `make sh` - Enter to backend
  * `php artisan migrate` - To migrate DB
## Work steps
* `make start` - Start services
* Working...
* `make stop` - Stop services
## Build project
* `make build` - Build for Production
