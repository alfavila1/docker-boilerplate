CREATE USER IF NOT EXISTS myuser@'%' IDENTIFIED BY 'password';
SET PASSWORD FOR myuser@'%' = PASSWORD('password');
CREATE DATABASE IF NOT EXISTS mydb SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
GRANT ALL ON mydb.* TO myuser@'%';

